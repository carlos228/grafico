package com.example.graficodofelipe2;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private LineChart chart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        chart = findViewById(R.id.Teste);
        //chart.setViewPortOffsets(0, 0, 0, 0);
        chart.setBackgroundColor(Color.rgb(104, 241, 175));

        List<Entry> lista = new ArrayList<>();

        int i=0,j=0;
        boolean crecente = true;
        while(i<100000){
            if(j == 50){
                crecente = false;
            }if(j == 0){
                crecente = true;
            }
            lista.add(new Entry(i,j));
            i++;
            if(crecente){
                j++;
            }else{
                j--;
            }
        }

        LineDataSet lineDataSet = new LineDataSet(lista,"Teste");
        //lineDataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        LineData lineData = new LineData(lineDataSet);

        chart.setData(lineData);
        chart.setScaleEnabled(true);
        chart.zoom(250,0,1,1);




        Toast.makeText(this,"Zom: "+chart.getX()+"Escala X: "+chart.getScaleX()+" Escala Y: "+chart.getScaleY(),Toast.LENGTH_LONG).show();
        //chart.invalidate();
    }
}
